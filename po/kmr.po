# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: mediaplayer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 13:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: kmr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: mediaplayer-app.desktop.in:4 mediaplayer-app.desktop.in:5
#: mediaplayer-app.desktop.in:6 src/mediaplayer.cpp:82
msgid "Media Player"
msgstr ""

#: mediaplayer-app.desktop.in:7
msgid "Movies;Movie Player;Video Player"
msgstr ""

#: mediaplayer-app.desktop.in:10
msgid "/usr/share/mediaplayer-app/mediaplayer-app.png"
msgstr ""

#: src/qml/player.qml:76
msgid "Error"
msgstr ""

#: src/qml/player.qml:77
msgid ""
"No video selected to play. Connect your phone to your computer to transfer "
"videos to the phone. Then select video from Videos scope."
msgstr ""

#: src/qml/player.qml:80
msgid "Ok"
msgstr ""

#: src/qml/player.qml:188
msgid "Play / Pause"
msgstr ""

#: src/qml/player.qml:189
msgid "Pause or Resume Playhead"
msgstr ""

#: src/qml/player.qml:193
msgid "Share"
msgstr ""

#: src/qml/player.qml:194
msgid "Post;Upload;Attach"
msgstr ""

#. TRANSLATORS: this refers to a duration/remaining time of the video, of which you can change the order.
#. %1 refers to hours, %2 refers to minutes and %3 refers to seconds.
#: src/qml/player/TimeLine.qml:55
#, qt-format
msgid "%1:%2:%3"
msgstr ""

#. TRANSLATORS: this refers to an unknown duration.
#: src/qml/player/TimeLine.qml:85
msgid "unknown"
msgstr ""

#: src/qml/player/TimeLine.qml:88
msgid "0:00:00"
msgstr ""

#: src/qml/player/VideoPlayer.qml:195
msgid "Please choose a file to open"
msgstr ""

#: src/qml/player/VideoPlayer.qml:298
msgid "Error playing video"
msgstr ""

#: src/qml/player/VideoPlayer.qml:302
msgid "Close"
msgstr ""

#: src/qml/player/VideoPlayer.qml:325
msgid "Fail to open the source video."
msgstr ""

#: src/qml/player/VideoPlayer.qml:328
msgid "Video format not supported."
msgstr ""

#: src/qml/player/VideoPlayer.qml:331
msgid "A network error occurred."
msgstr ""

#: src/qml/player/VideoPlayer.qml:334
msgid "You don't have the appropriate permissions to play a media resource."
msgstr ""

#: src/qml/player/VideoPlayer.qml:337
msgid "Fail to connect with playback backend."
msgstr ""
