/*
 * Copyright (C) 2012-2013 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Qt
#include <QGuiApplication>

// System
#include <libintl.h>

// local
#include "config.h"
#include "mediaplayer.h"

int main(int argc, char** argv)
{
    unsetenv("QML_FIXED_ANIMATION_STEP");

    QGuiApplication::setApplicationName("Media Player");
    MediaPlayer application(argc, argv);

    textdomain("lomiri-mediaplayer-app");
    std::string localeDir = i18nDir().toStdString();
    bindtextdomain("lomiri-mediaplayer-app", localeDir.c_str());
    bind_textdomain_codeset("lomiri-mediaplayer-app", "UTF-8");

    if (!application.setup()) {
        return 1;
    }

    return application.exec();
}
